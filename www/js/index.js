/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready

var deviceReady = false;
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    deviceReady = true;
}

function tomarFoto() {
    alert("VOY A TOMAR FOTO");
    console.log("VOY A TOMAR FOTO");

    if(!deviceReady){
        return;
    }

    navigator.camera.getPicture(
        function(uri){
            var image = document.getElementById("contenedor");
            image.src = uri;
        }, 
        function(mensaje){
            alert("ERROR AL TOMAR FOTO: " + mensaje);            
        }, 
        { quality: 50, destinationType: Camera.DestinationType.FILE_URI }
        );
}

function leerQR() {
    alert("VOY A LEER QR");

    if(!deviceReady){
        return;
    }
    
    /*
    cordova.plugins.barcodeScanner.scan(
      function (result) {
          alert("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);
      },
      function (error) {
          alert("Scanning failed: " + error);
      },
      {
          preferFrontCamera : true, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: true, // Android, launch with the torch switched on (if available)
          saveHistory: true, // Android, save scan history (default false)
          prompt : "Place a barcode inside the scan area", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS and Android
      }
   );
   */

    cordova.plugins.barcodeScanner.scan(
        function(resultado){
            alert("SE ENCONTRO: " + resultado.text + " CON FORMATO: " + resultado.format);
        },
        function(mensaje){
            alert("HUBO ERROR EN EJECUCION: " + mensaje);
        },
        {preferFrontCamera : true, showFlipCameraButton : true, prompt : "Busca QR!", orientation : "portrait", disableSuccessBeep: false}
    );

}

function vibrar() {
    
    alert("VOY A VIBRAR");

    if(!deviceReady)
        return;

    navigator.vibrate([1000, 1000, 3000, 1000, 5000]);
}

